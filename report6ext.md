# 1

Dịch mã đối tượng có gọi thủ tục, cấp phát tĩnh.

## Định vị địa chỉ cho các biến

Coi mỗi biến chiếm 4 ô địa chỉ.

```nasm
; 100 + 30 - 4*4 = 114
#define x 114
#define a 118
#define b 122
#define c 126

; 300 + 40 - 5*4 = 320
#define y 320
#define d 324
#define e 328
#define f 332
#define g 336
```

## Sinh mã

Chia thành các khối cơ bản:

```
// S
//----1------
x := (a+b)*c
a := x*2
//----2------
call P
//----3------
halt

// P
//----4------
y := (d-e)/f + g
return

```

## Khối 1:

<!-- https://graphs.grevian.org/graph/6296194525429760 -->

![](6ext1.png)

```
gen(a *)                   ; 0 c1
  gen(x *)                 ;   c1
    gen(t1 +)              ;   c1
      gen(a)               ;   c0
        print MOV a, R0
      print ADD b, R0
    print MUL c, R0
    print MOV R0, x        ;   commit x
  print MUL #2, R0
  print MOV R0, a          ;   commit a
```

Output - chiếm 2*6*4 = 48 byte:

```nasm
MOV a, R0
ADD b, R0 
MUL c, R0 
MOV R0, x 
MUL #2, R0
MOV R0, a 
```

## Khối 4:

<!-- https://graphs.grevian.org/graph/5184866066890752 -->

![](6ext4.png)

```
gen(y +)            ; 0 c1
  gen(t2 /)         ;   c1
    gen(t1 -)       ;   c1
      gen(d)        ;   c0
        MOV d, R0
      SUB e, R0
    DIV f, R0
  ADD g, R0
  MOV R0, y         ;   commit
```

Output - chiếm 2*5*4 = 40 byte:

```nasm
MOV d, R0
SUB e, R0
DIV f, R0
ADD g, R0
MOV R0, y
```

Thêm lệnh return là 48 byte

## Kết quả

Đặt mã cho S tại địa chỉ 0 => `call P` tại 48, địa chỉ khứ hồi tại 68.
Chỉ thị `HALT` giá là 1, vậy mã cho S trải dài 0 -> 71. Như vậy phần còn lại
từ 72 -> 99 đã không đủ chứa code cho P. Ta chọn code cho P tại 200.

```nasm
#define x 114
#define a 118
#define b 122
#define c 126

#define y 320
#define d 324
#define e 328
#define f 332
#define g 336

.ORIG 0

MOV a, R0
ADD b, R0 
MUL c, R0 
MOV R0, x 
MUL #2, R0
MOV R0, a 

MOV #68, 300
GOTO 200

HALT

.ORIG 200

MOV d, R0
SUB e, R0
DIV f, R0
ADD g, R0
MOV R0, y

GOTO *300

```

# 2

Dịch mã đối tượng có gọi thủ tục, cấp phát động cơ chế stack.

## Sinh mã

Chia thành các khối cơ bản:

```
// S
//----1--------
x := a+b*c
y := x/d-e
//----2--------
call Q
//----3--------
a := b*(c+e)
z := a*2

// P
//----4--------
m := y + 9*(c-d)
return

// Q
//----5--------
x := (a-b)/c + d
//----6--------
call P
//----7--------
return
```

## Khối 1

<!-- https://graphs.grevian.org/graph/6290772766752768 -->

![](6ext21.png)

```
gen(y -)
  gen(t2 /)
    gen(x +)
      gen(a)
        print MOV a, R0
      R = pop()           ; R0, top == R1
      gen(t1 *)
        gen(b)
          print MOV b, R1
        print MUL c, R1
      print ADD R1, R0
      push(R)             ; R0, top == R0
      print MOV R0, x     ; commit x
    print DIV d, R0
  print SUB e, R0
  print MOV R0, y         ; commit y
```

Output - chiếm `(7*2 + 1)*4 = 60 byte < 200 byte`, ổn :

```nasm
MOV a, R0
MOV b, R1
MUL c, R1
ADD R1, R0
MOV R0, x
DIV d, R0
SUB e, R0
MOV R0, y
```

## Khối 3

<!-- https://graphs.grevian.org/graph/5076877033078784 -->

![](6ext23.png)

```
gen(z *)
  gen(a *)
    gen(b)
      print MOV b, R0
    R = pop()     ; R0, top == R1
    gen(t1 +)
      gen(c)
        print MOV c, R1
      print ADD e, R1
    print MUL R1, R0
    push(R)       ; R0, top == R0
    print MOV R0, a     ; commit a
  print MUL #2, R0
  print MOV R0, z       ; commit z
```

Output - chiếm `(6*2 + 1)*4 = 52 byte < 200 byte`, ổn:

```nasm
MOV b, R0
MOV c, R1
ADD e, R1
MUL R1, R0
MOV R0, a
MUL #2, R0
MOV R0, z
```

## Khối 4

<!-- https://graphs.grevian.org/graph/5190382381957120 -->

![](6ext24.png)

```
gen(m +)         ; rstack: 10
  swap()         ; rstack: 01
  gen(t2 *)
    gen(#9)
      print MOV #9, R1
    R = pop()    ; R1, rstack: 0
    gen(t1)
      gen(c)
        print MOV c, R0
      print SUB d, R0
    print MUL R0, R1
    push(R)      ; R1, rstack: 01
  R = pop()      ; R1, rstack: 0
  gen(y)
    print MOV y, R0
  print ADD R1, R0
  push(R)        ; R1, rstack: 01
  swap()         ; rstack: 10
  print MOV R0, m      ; commit m
```

Output - chiếm `(5*2 + 2*1) * 4 = 48 byte < 200 byte`, ổn:

```nasm
MOV #9, R1
MOV c, R0
SUB d, R0
MUL R0, R1
MOV y, R0
ADD R1, R0
MOV R0, m
```

## Khối 5

<!-- https://graphs.grevian.org/graph/5162576931454976 -->

![](6ext25.png)

```
gen(x +)
  gen(t2 /)
    gen(t1 -)
      gen(a)
        print MOV a, R0
      print SUB b, R0
    print DIV c, R0
  print ADD d, R0
  print MOV R0, x
```

Output - chiếm `5*2*4 = 40 byte < 200 byte`, ổn:

```nasm
MOV a, R0
SUB b, R0
DIV c, R0
ADD d, R0
MOV R0, x
```

## Kết quả

Không có thông tin cho răng S, P, hay Q là entry point
của chương trình. Giả sử entry point tại địa chỉ 0.

Cũng giả sử bản ghi hoạt động của S, P, Q chứa toàn bộ các tên biến được sử dụng trong mã trung gian tương ứng của nó, theo đúng thứ tự xuất hiện trong mã trung gian, bắt đầu từ `4(SP)` (tại SP đã lưu địa chỉ khứ hồi)

Ta có địa chỉ tương đối của các tên biến như sau:

```
# S
x: 4
a: 8
b: 12
c: 16
y: 20
d: 24
e: 28
z: 32

# P
m: 4
y: 8
c: 12
d: 16

# Q
x: 4
a: 8
b: 12
c: 16
d: 20
```

```nasm
.ORIG 0        ; entry point
MOV #800, SP   ; enable stack
;; more code ;;

.ORIG 200      ; S

MOV *8(SP), R0
MOV *12(SP), R1
MUL *16(SP), R1
ADD R1, R0
MOV R0, 4(SP)
DIV *24(SP), R0
SUB *28(SP), R0
MOV R0, 20(SP)

ADD Ssize, SP
MOV PC, R0
ADD #16, R0
MOV R0, *SP
GOTO 600
SUB Ssize, SP

MOV *12(SP), R0
MOV *16(SP), R1
ADD *28(SP), R1
MUL R1, R0
MOV R0, 8(SP)
MUL #2, R0
MOV R0, 32(SP)

GOTO *SP      ; return to caller of S

.ORIG 400     ; P

MOV #9, R1
MOV *12(SP), R0
SUB *16(SP), R0
MUL R0, R1
MOV *8(SP), R0
ADD R1, R0
MOV R0, 4(SP)

GOTO *SP      ; return to caller of P (Q)

.ORIG 600     ; Q

MOV *8(SP), R0
SUB *12(SP), R0
DIV *16(SP), R0
ADD *20(SP), R0
MOV R0, 4(SP)

ADD Qsize, SP
MOV PC, R0
ADD #16, R0
MOV R0, *SP
GOTO 600
SUB Qsize, SP

GOTO *SP       ; return to caller of Q (S)

```