# Bài tập chương 6

## 6.2 Dịch code C ra mã đối tượng dùng thủ tục gencode

### Lý thuyết

Bài toán:

Nhập: code C  
Xuất: mã đối tượng

B1: Dịch ra mã trung gian  
B2: Xây dựng DAG  
B3: Xếp thứ tự DAG (đánh số)  
B4: Chia DAG thành một lớp các cây con (nếu có nút dùng chung)  
B5: Gắn nhãn cho các cây  
B6: Duyệt các cây thực hiện gencode (thứ tự cây là thứ tự đã xếp của các gốc tương ứng)  

Dễ dàng nhận thấy trong bài 6.1, các DAG đều là cây sẵn rồi. Vậy thuật toán trên đơn giản lại như sau (bỏ B4):

B1: Dịch ra mã trung gian  
B2: Xây dựng DAG  
B3: Xếp thứ tự DAG (đánh số)  
B5: Gắn nhãn cho DAG  
B6: Duyệt DAG thực hiện gencode  

Note: Về toán tử gán dạng `a := b` (b là label) ta có thể phân tích thành cây với nút cha `:=` và 2 nút con `a`, `b`; tuy nhiên như vậy khi gencode sẽ có chỉ thị load a từ bộ nhớ vào thanh ghi, là chỉ thị thừa. Ở đây ta cho a là nút cha với `a.commit = 1`, khi đó `gencode(a)` sẽ sinh ra `MOV top(rstack), a`. Ta cũng sẽ commit tất cả các tên biến là output của khối cơ bản.

### Bài làm

Quy ước:

* Ta đặt lại gencode thành gen cho gọn.
* c3 : condition 3 : trường hợp thứ 3 của thuật toán gencode
* l1 : label 1 : nhãn 1 trên cây phân tích
* `push()`, `pop()`, `swap()` là thao tác trên rstack
* `gen(x +)`: gencode cho tên biến là `x` với toán tử là `+`

#### a

<!-- https://graphs.grevian.org/graph/5698636866387968 -->

![](62a.png)

```
gen(x)                  ; 0 <- số hiệu thanh ghi ở đỉnh stack
                        ;   c3
  gen(#1)               ; 0 c0
    print MOV #1, R0    ; 0
  print MOV R0, x       ; 0 commit
```

Output:

```nasm
MOV #1, R0
MOV R0, x
```

#### b

<!-- https://graphs.grevian.org/graph/4845013894692864 -->

![](62b.png)

```
gen(x)                  ; 0 c3
  gen(y)                ; 0 c0
    print MOV y, R0     ; 0
  print MOV R0, x       ; 0 commit
```

Output:

```nasm
MOV y, R0
MOV R0, x
```

#### c

<!-- https://graphs.grevian.org/graph/6290586438991872 -->

![](62c.png)

```
gen(x +)                ; 0 c1
  gen(x)                ; 0 c0
    print MOV x, R0     ; 0
  print ADD #1, R0      ; 0
  print MOV R0, x       ; 0 commit
```

Output:

```nasm
MOV x, R0
ADD #1, R0
MOV R0, x
```

#### d

<!-- https://graphs.grevian.org/graph/5662397945610240 -->

![](62d.png)

```
gen(x /)                         ; 0 c2
  gen(a)                         ; 0 c0
    print MOV a, R0

  R = pop(rstack) ; R0           ; 1

  gen(t1 +)                      ; 1 c1
    gen(b)                       ; 1 c0
      print MOV b, R1
    print ADD c, R1

  print DIV R1, R0

  push(R)                        ; 0

  print MOV R0, x                ; 0 commit
```

Output:

```nasm
MOV a, R0
MOV b, R1
ADD c, R1
DIV R1, R0
MOV R0, x
```

#### e

<!-- https://graphs.grevian.org/graph/5661266892816384 -->

![](62e.png)

```
gen(x -)                   ; 0 c3
  gen(t2 /)                ; 0 c3
    gen(a)                 ; 0 c0
      print MOV a, R0
    R = pop() ; R0         ; 1
    gen(t1 +)              ; 1 c1
      gen(b)               ; 1 c0
        print MOV b, R1
      print ADD c, R1
    print DIV R1, R0
    push(R)                ; 0
  R = pop() ; R0           ; 1
  gen(t4)                  ; 1 c3
    gen(d)                 ; 1 c0
      print MOV d, R1
    R = pop() ; R1         ; 2
    gen(t3)                ; 2 c1
      gen(e)               ; 2 c0
        print MOV e, R2
      print ADD f, R2
    print MUL R2, R1
    push(R)                ; 1
  print SUB R1, R0
  push(R=R0)               ; 0
  print MOV R0, x          ; 0 commit
```

Output:

```nasm
MOV a, R0
MOV b, R1
ADD c, R1
DIV R1, R0
MOV d, R1
MOV e, R2
ADD f, R2
MUL R2, R1
SUB R1, R0
MOV R0, x
```