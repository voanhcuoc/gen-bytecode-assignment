# Bài tập chương 6

## 6.1 Dịch code C ra mã đối tượng

a) `x = 1`

```asm
MOV #1, x
```

b) `x = y`

```asm
MOV x, R0
MOV R0, y
```

c) `x = x + 1`

```asm
MOV x, R0
ADD #1, R0
MOV R0, x
```

d) `x = a + b * c`

C có thứ hạng toán tử , `*` trước `+` sau.

```asm
MOV b, R0
MUL c, R0
MOV a, R1
ADD R0, R1
MOV R1, x
```

e) 

```
x = a/(b+c) - d*(e+f)
      t1        t2
```

```asm
MOV b, R0
ADD c, R0
MOV a, R1
DIV R0, R1 // R1 chứa t1
MOV e, R0
ADD f, R0
MOV d, R2
MUL R0, R2 // R2 chứa t2
SUB R2, R1 // R1 chứa x
MOV R1, x
```

Nhận xét: áp dụng tính chất đại số như giao hoán ta có thể đạt được mã đối tượng tốt hơn